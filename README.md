# mongo_connections

Processes MongoDB log files and creates JSON structures with information on connections.


```angular2html
usage: connection_duration.py [-h] [--input-file INPUT_FILE] [--input-files INPUT_FILES] [--duration] [--errors] [--start] [--end] [--prefix PREFIX]
                              [--driver DRIVER] [--auth]

get connection duration and drivers used in a mongodb.log

options:
  -h, --help            show this help message and exit
  --input-file INPUT_FILE
                        Path to single mongodb log file.
  --input-files INPUT_FILES
                        Path to multiple mongodb log files.
  --duration            Output duration of each connection in milliseconds
  --errors              Output errors in connections.
  --start               Output start of each connection.
  --end                 Output end of each connection.
  --prefix PREFIX       The prefix for the file names used for logs. Defaults to current timestamp.
  --driver DRIVER       Search for specific driver.
  --auth                Authentication users.

```

