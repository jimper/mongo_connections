#!/usr/bin/env python3

"""

"""



from datetime import datetime
import glob
import json
from time import time
from sys import stdout

OP_AUTH = 'auth'
OP_RE_AUTH = 're_auth'
OP_RECV = 'recv():'
OP_RECEIVED = 'received'
OP_END = 'end'
OP_ERROR = 'Error'


def print_progress_line(s):
    for x in range(len(s) + 1): stdout.write("\x08")
    stdout.write(s)
    stdout.flush()


def get_durations(conn_lines, driver=None):
    _durations = []
    doc_index = 0
    for doc_index in range(len(conn_lines)):

        if (doc_index % 1000) == 0:
            print_progress_line(f'line {doc_index}/{len(conn_lines)}')

        start = conn_lines[doc_index]

        if start['op'] == OP_RECEIVED:
            for i in range(doc_index, len(conn_lines)):

                end = conn_lines[i]
                if end['op'] == OP_END and start['conn'] == end['conn']:
                    delta = datetime.fromisoformat(end['t']) - datetime.fromisoformat(start['t'])
                    out_document = {
                        'conn':  start['conn'],
                        'client': start['client'],
                        'driver': start['driver'],
                        'version': start['version'],
                        'start': str(start),
                        'end': end['t'],
                        'duration': int(delta.seconds * 1000 + delta.microseconds / 1000)
                    }
                    if not driver or (driver and driver in start['driver']):
                        _durations.append(out_document)
                    break

    print_progress_line(f'line {doc_index}/{len(conn_lines)}\n')

    return _durations


def get_errors(conn_lines):
    _errors = []
    i = 0
    for conn in conn_lines:
        i += 1
        if (i % 1000) == 0:
            print_progress_line(f'line {i}/{len(conn_lines)}')

        if conn['op'] == OP_ERROR or conn['op'] == OP_RECV:
            error_doc = conn
            error_doc['t'] = str(conn['t'])

            _errors.append(error_doc)
    return _errors


def get_starts(conn_lines):
    _starts = []
    i = 0
    for conn in conn_lines:
        i += 1
        if (i % 1000) == 0:
            print_progress_line(f'line {i}/{len(conn_lines)}')

        if conn['op'] == OP_RECEIVED:
            start_doc = conn
            start_doc['t'] = str(conn['t'])

            _starts.append(start_doc)
    return _starts


def get_ends(conn_lines):
    _ends = []
    i = 0
    for conn in conn_lines:
        i += 1
        if (i % 1000) == 0:
            print_progress_line(f'line {i}/{len(conn_lines)}')

        if conn['op'] == OP_END:
            end_doc = conn
            end_doc['t'] = str(conn['t'])
            _ends.append(end_doc)
    return _ends

def get_auth(conn_lines):
    _auth = []
    i = 0
    for conn in conn_lines:
        i += 1
        if (i % 1000) == 0:
            print_progress_line(f'line {i}/{len(conn_lines)}')

        if conn['op'] == OP_AUTH or conn['op'] == OP_RE_AUTH:
            auth_doc = conn
            _auth.append(auth_doc)
    return _auth


def decode_old_log(line, driver=None):
    token = line.split()
    if len(token) > 4:
        if token[2] == 'NETWORK' and '[conn' in token[3]:

            #
            conn_doc = {
                'conn': token[3][1:-1],
                't': datetime.fromisoformat(
                    token[0].replace('T', ' ')[:-5]),
                'op': token[4]
            }
            driver_start = None
            #
            if conn_doc['op'] == OP_RECEIVED:
                conn_doc['client'] = token[8]

                platform = str.join(' ', token[20:]).replace('}', '')
                conn_doc['platform'] = platform

                version_found = token[16].replace('"', '')
                driver_found = token[14].replace('"', '').replace(',', '')

                conn_doc['driver'] = driver_found
                conn_doc['version'] = version_found

                if driver and driver not in driver_found:
                    return None, None

                driver_start = {
                    'start': token[0],
                    'conn': token[3].replace('[', '').replace(']', ''),
                    'client': token[6],
                    'line': line
                }
            elif conn_doc['op'] == OP_END:
                conn_doc['client'] = token[6]
                conn_doc[OP_END] = token
            elif conn_doc['op'] == OP_ERROR:
                conn_doc['line'] = line
            elif conn_doc['op'] == OP_RECV:
                conn_doc['line'] = line
            else:
                conn_doc['unknown'] = line

            return conn_doc, driver_start

    return None, None


def decode_json(line, driver=None):
    doc_line = json.loads(line)
    if doc_line['c'] == 'ACCESS':
        tt = doc_line['t']['$date'].replace('T', ' ')[:-6]

        conn = None
        if 'reauthenticate' in doc_line['msg']:
            conn = {
                'op': OP_RE_AUTH,
                't': tt,
                'conn': doc_line['ctx'],

                'principal': doc_line['attr']['user']['user'],
                'authDB': doc_line['attr']['user']['db'],
            }
        elif 'Authentication' in doc_line['msg']:
            conn = {
                'op': OP_AUTH,
                't': tt,
                'client': doc_line['attr']['remote'],
                'conn': doc_line['ctx'],
                'mechanism': doc_line['attr']['mechanism'],
                'principal': doc_line['attr']['principalName'],
                'remote': doc_line['attr']['remote'],
                'authDB': doc_line['attr']['authenticationDatabase'],
                'extra_info': doc_line['attr']['extraInfo'],
            }
        return conn, None
    elif doc_line['c'] == 'NETWORK' and 'conn' in doc_line['ctx']:
        conn = None
        driver_start = None

        if doc_line['msg'] == 'client metadata':
            drv = doc_line['attr']['doc']['driver']
            tt = doc_line['t']['$date'].replace('T', ' ')[:-6]

            if driver and driver not in drv['name']:
                return None, None

            conn = {
                'op': OP_RECEIVED,
                't': tt,
                'client': doc_line['attr']['remote'],
                'conn': doc_line['ctx'],
                'driver': drv['name'],
                'version': drv['version'],
                'line': str(doc_line)
            }
            driver_start = {
                'start': tt,
                'conn': doc_line['ctx'],
                'client': doc_line['attr']['remote'],
                'line': str(doc_line)
            }

        elif doc_line['msg'] == 'Connection ended':
            tt = doc_line['t']['$date'].replace('T', ' ')[:-6]
            conn = {
                'op': OP_END,
                't': tt,
                'client': doc_line['attr']['remote'],
                'conn': doc_line['ctx'],
                # 'end': datetime.fromisoformat(tt),
            }
        elif 'Error' in doc_line['msg']:
            tt = doc_line['t']['$date'].replace('T', ' ')[:-6]
            conn = {
                'op': OP_ERROR,
                't': tt,
                'client': doc_line['attr']['remote'],
                'conn': doc_line['ctx'],
                'line': str(doc_line['attr']['error'])
            }
        elif 'Shutdown' in doc_line['msg']:
            tt = doc_line['t']['$date'].replace('T', ' ')[:-6]
            conn = {
                'op': OP_ERROR,
                't':tt,
                'line': doc_line['msg'],
                'conn': doc_line['ctx'],

            }

        return conn, driver_start

    return None, None


def get_drivers(file_name, prev_dict=None, prev_conn_lines=None, driver=None):
    if prev_dict:
        driver_dict = prev_dict
    else:
        driver_dict = dict([])

    if prev_conn_lines:
        all_conn_lines = prev_conn_lines
    else:
        all_conn_lines = []

    try:
        with open(file_name, 'rt') as f:
            i = 0
            line = f.readline()
            while line:

                if (i%1000) == 0:
                    print_progress_line(f'reading line {i}')

                try:
                    conn_line, driver_start = decode_json(line, driver)
                except json.JSONDecodeError:
                    conn_line, driver_start = decode_old_log(line, driver)

                if conn_line:
                    all_conn_lines.append(conn_line)

                    if driver_start:
                        key = conn_line['driver'] + ' ' + conn_line['version']
                        driver_dict[key] = driver_dict.get(key, {'count': 0})
                        _count = driver_dict[key]['count']
                        if _count == 0:
                            driver_dict[key]['count'] = 1
                        else:
                            driver_dict[key]['count'] += 1
                # next line
                line = f.readline()
                i += 1

            f.close()
            print_progress_line(f'reading line {i}')

        return driver_dict, all_conn_lines
    except FileNotFoundError:
        return None, None


def get_drivers_in_path(input_files, driver=None):
    stdout.write(f'Reading {input_files}\n')

    try:
        files = glob.glob(input_files)

        if len(files) > 0:
            files.sort()
            if files[0].endswith('mongodb.log'):
                first = files[0]
                files.pop(0)
                files.append(first)

            conns = []
            drv = dict([])

            for file in files:
                drv, conns = get_drivers(file, drv, conns, driver=driver)

            return drv, conns
        else:
            return None, None

    except FileNotFoundError:
        return None, None


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description='get connection duration and drivers used in a mongodb.log')
    parser.add_argument('--input-file', required=False,
                        help='Path to single mongodb log file.', type=str)
    parser.add_argument('--input-files', required=False,
                        help='Path to multiple mongodb log files.', type=str)
    parser.add_argument('--duration', required=False, default=False, action='store_true',
                        help='Output duration of each connection in milliseconds')
    parser.add_argument('--errors', required=False, default=False, action='store_true',
                        help='Output errors in connections.')
    parser.add_argument('--start', required=False, default=False, action='store_true',
                        help='Output start of each connection.')
    parser.add_argument('--end', required=False, default=False, action='store_true',
                        help='Output end of each connection.')
    parser.add_argument('--prefix', default=None, type=str,
                        help='The prefix for the file names used for logs. Defaults to current timestamp.')
    parser.add_argument('--driver', required=False, default=None,
                        help='Search for specific driver.')
    parser.add_argument('--auth', required=False, default=False, action='store_true',
                        help='Authentication users.')

    args = parser.parse_args()

    if not args.prefix:
        args.prefix = str(int(time()))

    driver_list = None
    connections_list = None
    if args.input_file:
        driver_list, connections_list = get_drivers(args.input_file, driver=args.driver)
    if args.input_files:
        driver_list, connections_list = get_drivers_in_path(args.input_files, driver=args.driver)

    if driver_list and connections_list:
        stdout.write('\nDriver versions\n')
        with open(f'{args.prefix}drivers.json', 'wt') as f:
            for k in driver_list.keys():
                count = driver_list[k]['count']
                f.write(f'{{ "{k}": "{count}" }}\n')
            f.close()

    else:
        stdout.write(f'Could not read connection info from {args.input_file}\n')
        exit(2)

    errors = None
    starts = None
    ends = None
    if args.errors:
        stdout.write('\nErrors\n')
        errors = get_errors(connections_list)
        with open(f'{args.prefix}errors.json', 'wt') as f:
            for e in errors:
                f.write(f'{json.dumps(e)}\n')
            f.close()

    if args.start:
        stdout.write('\nStarts\n')
        starts = get_starts(connections_list)
        with open(f'{args.prefix}start.json', 'wt') as f:
            for s in starts:
                f.write(f'{json.dumps(s)}\n')
            f.close()

    if args.end:
        stdout.write('\nEnds\n')
        ends = get_ends(connections_list)
        with open(f'{args.prefix}end.json', 'wt') as f:
            for e in ends:
                f.write(f'{json.dumps(e)}\n')
            f.close()

    if args.duration:
        stdout.write('\nDuration\n')
        durations = get_durations(connections_list, driver=args.driver)
        with open(f'{args.prefix}duration.json', 'wt') as f:
            for d in durations:
                f.write(f'{json.dumps(d)}\n')
            f.close()

    if args.auth:
        stdout.write('\nAuthentication\n')
        auth = get_auth(connections_list)
        with open(f'{args.prefix}auth.json', 'wt') as f:
            for a in auth:
                f.write(f'{json.dumps(a)}\n')
            f.close()
